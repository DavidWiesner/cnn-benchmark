from __future__ import print_function
# from __future__ import absolute_import
import argparse
import itertools
import os
import signal
import sys
import time
import traceback

# from tf_cnn_benchmarks import benchmark_storage


current_params = []
log_file = None
use_pytorch = '--pytorch' in sys.argv

if use_pytorch:  #
    import pt_cnn_benchmarks.pt_cnn_benchmarks as cnn_benchmarks
    import torch.cuda as cuda

    ResourceExhaustedError = KeyboardInterrupt
    import pt_cnn_benchmarks.flags as flags


    def get_available_pus(device_type='GPU'):
        return range(cuda.device_count())
else:
    import tensorflow as tf

    flags = tf.flags

    import tf_cnn_benchmarks.tf_cnn_benchmarks as cnn_benchmarks
    from tensorflow.python.client import device_lib
    from tensorflow.python.framework.errors_impl import ResourceExhaustedError


    def get_available_pus(device_type='GPU'):
        devices = device_lib.list_local_devices()
        return [x.name for x in devices if x.device_type == device_type]


def signal_handler(signal, frame):
    global log_file
    if log_file is not None:
        log_file.close()
        log_file = None
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)


def store(data, type=''):
    params = ', '.join(['%20s' % str(x)[:20] for x in current_params])
    log_line = '%8.2f, ' % data['total_images_per_sec'] + params
    if log_file is not None:
        log_file.write(log_line + '\n')
    print(log_line)


flags.DEFINE_string('log_file', None, """file to log the stats""")
flags.DEFINE_boolean('verbose', False, """verbose outputs""")
flags.DEFINE_integer('continue_with', -1, """verbose outputs""")
FLAGS = flags.FLAGS

# benchmark_storage.store_benchmark = store
cnn_benchmarks.store_benchmarks = store

if not FLAGS.verbose:
    cnn_benchmarks.log_fn = lambda *args: args


def run_bench(params):
    global current_params
    try:
        current_params = params
        if not use_pytorch and flags.FLAGS.winograd_nonfused:
            os.environ['TF_ENABLE_WINOGRAD_NONFUSED'] = '1'
        else:
            os.environ.pop('TF_ENABLE_WINOGRAD_NONFUSED', None)
        if not use_pytorch and flags.FLAGS.autotune_threshold:
            os.environ['TF_AUTOTUNE_THRESHOLD'] = str(flags.FLAGS.autotune_threshold)
        if not use_pytorch:
            os.environ['TF_SYNC_ON_FINISH'] = str(int(flags.FLAGS.sync_on_finish))
        argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        bench = cnn_benchmarks.BenchmarkCNN()
        if FLAGS.verbose:
            bench.print_info()
        bench.run()
    except ResourceExhaustedError as e:
        print(traceback.format_exc(), file=sys.stderr)
        store({'total_images_per_sec': -1})
    except RuntimeError as e:
        print(traceback.format_exc())
        if e.args[0].startswith('cuda runtime error (2) : out of memory'):
            store({'total_images_per_sec': -1})
        else:
            store({'total_images_per_sec': -2})
    except Exception as e:
        print(traceback.format_exc(), file=sys.stderr)
        store({'total_images_per_sec': -2})


def main(_):
    global log_file
    gpus = len(get_available_pus())
    if FLAGS.log_file is not None:
        log_file = open(FLAGS.log_file, 'a' if FLAGS.continue_with > 0 else 'w', 1)
    FLAGS.local_parameter_device = 'gpu'
    FLAGS.num_gpus = 1
    FLAGS.batch_size = 32
    FLAGS.variable_update = 'parameter_server'
    FLAGS.use_nccl = False
    FLAGS.result_storage = 'blah'
    models = ['alexnet', 'inception4', 'resnet50', 'resnet101', 'resnet152', 'vgg16']
    if use_pytorch:
        models[1] = 'inception_v3'
    updates = ['parameter_server', 'replicated', 'independent'] if not use_pytorch else ['replicated']
    batch_sizes = [16, 32, 64, 128]
    gpus_sizes = range(1, gpus + 1) if gpus > 0 else [0]
    use_nccl = [True, False] if gpus > 0 and not use_pytorch else [False]
    if 'TF_TRIVIAL' in os.environ:
        models = ['trivial']
        batch_sizes = [16]

    prod = itertools.product(models, updates, batch_sizes, gpus_sizes, use_nccl)
    for cnt, params in enumerate(prod):
        if (cnt + 1) < FLAGS.continue_with:
            print('continue_with was set to %d skip step %d' % (FLAGS.continue_with, cnt + 1))
            continue
        model, variable_update, batch_size, num_pus, nccl = params
        FLAGS.model = model
        FLAGS.variable_update = variable_update
        FLAGS.batch_size = batch_size
        FLAGS.num_gpus = num_pus
        FLAGS.use_nccl = nccl
        if num_pus == 0:
            FLAGS.device = 'cpu'
            FLAGS.num_gpus = 1
            FLAGS.data_format = 'NHWC'
        run_bench(params)
        time.sleep(3)
    if log_file is not None:
        log_file.close()
        log_file = None


if __name__ == '__main__':
    if use_pytorch:  #
        main('')
    else:
        import tensorflow

        tensorflow.app.run()
