from __future__ import print_function

import time

# from __future__ import absolute_import
import numpy as np
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim
import torchvision.models as models
from torch.autograd import Variable
from torchvision import datasets, transforms

from . import flags

model_names = sorted(name for name in models.__dict__
                     if name.islower() and not name.startswith("__")
                     and callable(models.__dict__[name]))
print(model_names)
flags.DEFINE_integer('display_every', 10,
                     """Number of local steps after which progress is printed
                     out""")

flags.DEFINE_string('model', 'trivial', 'name of the model to run')
flags.DEFINE_integer('num_gpus', 1, 'the number of GPUs to run on')
flags.DEFINE_string('device', 'gpu',
                    """Device to use for computation: cpu or gpu""")
flags.DEFINE_integer('batch_size', 32, 'batch size per compute device')
flags.DEFINE_integer('num_warmup_batches', None,
                     'number of batches to run before timing')
flags.DEFINE_integer('autotune_threshold', None,
                     'The autotune threshold for the models')
flags.DEFINE_integer('num_batches', 100,
                     'number of batches to run, excluding warmup')
flags.DEFINE_string('data_dir', None, """Path to dataset in TFRecord format
                       (aka Example protobufs). If not specified,
                       synthetic data will be used.""")
flags.DEFINE_integer('workers', 4,
                     """number of data loading workers (default: 4)""")

FLAGS = flags.FLAGS
FLAGS.cuda = FLAGS.device == 'gpu' and torch.cuda.is_available()

log_fn = print  # tf.logging.info


class TrivialNet(nn.Module):
    def __init__(self, num_classes=1000):
        super(TrivialNet, self).__init__()
        self.classifier = nn.Sequential(
            nn.Linear(224 * 224 * 3, 1),
            nn.ReLU(inplace=True),
            nn.Linear(1, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, num_classes),
        )

    def forward(self, x):
        x = x.view(x.size(0), 224 * 224 * 3)
        x = self.classifier(x)
        return x


def cc(value):
    if FLAGS.cuda:
        return value.cuda()
    return value


def store_benchmarks(names_to_values):
    pass


class BenchmarkCNN(object):
    """Class for benchmarking a cnn network."""

    def __init__(self):
        self.model_name = FLAGS.model
        self.num_batches = FLAGS.num_batches
        autotune_threshold = FLAGS.autotune_threshold if (
            FLAGS.autotune_threshold) else 1
        min_autotune_warmup = 5 * autotune_threshold * autotune_threshold
        self.num_warmup_batches = FLAGS.num_warmup_batches if (
            FLAGS.num_warmup_batches) else max(10, min_autotune_warmup)
        self.num_gpus = FLAGS.num_gpus
        self.cuda = FLAGS.device == 'gpu' and torch.cuda.is_available()
        self.batch_size = FLAGS.batch_size * FLAGS.num_gpus
        self.raw_devices = ['%s:%i' % (FLAGS.device, i + 1)
                            for i in range(self.num_gpus)]
        self.data_dir = FLAGS.data_dir
        self.train_dir = FLAGS.data_dir + '/train' if FLAGS.data_dir is not None else None
        self.workers = FLAGS.workers
        self.image_size = 224 if self.model_name != 'inception_v3' else 299

    def _get_data_loader(self):
        if self.data_dir is not None:
            dataset = datasets.ImageFolder(self.train_dir, transforms.Compose(
                [transforms.RandomSizedCrop(self.image_size), transforms.RandomHorizontalFlip(), transforms.ToTensor()]))
        else:
            dataset = torch.utils.data.TensorDataset(
                torch.randn(self.batch_size, 3, self.image_size, self.image_size),
                torch.randn(self.batch_size).uniform_(0, 999).type(torch.LongTensor)
            )
            from torch.utils.data import DataLoader
        return torch.utils.data.DataLoader(
            dataset,
            batch_size=self.batch_size, shuffle=True,
            num_workers=self.workers, pin_memory=FLAGS.cuda)

    def _build_model(self):
        manual_seed()
        self.input = Variable(cc(torch.randn(self.batch_size, 3, 224, 224)))
        self.target = Variable(cc(torch.LongTensor(self.batch_size).fill_(1)))
        self.loss = cc(nn.CrossEntropyLoss())
        manual_seed()

        print("=> creating model '{}'".format(self.model_name))
        if self.model_name.startswith('trivial'):
            model = TrivialNet()
        else:
            model = models.__dict__[FLAGS.model]()
        if self.cuda:
            device_ids = list(range(self.num_gpus))
            if self.model_name.startswith('alexnet') or self.model_name.startswith('vgg'):
                model.features = torch.nn.DataParallel(model.features, device_ids=device_ids)
            else:
                model = torch.nn.DataParallel(model, device_ids=device_ids)
        if self.cuda:
            model.cuda()
        self.model = model

        self.optimizer = torch.optim.SGD(model.parameters(), 0.001,
                                         momentum=0.9,
                                         weight_decay=1e-5)

    def run(self):
        self._build_model()
        data_loader = self._get_data_loader()
        log_fn('Running warm up')
        local_step = -1 * self.num_warmup_batches
        step_train_times = []
        done_fn = lambda: local_step == self.num_batches
        while not done_fn():
            for input, target in data_loader:
                if self.cuda:
                    torch.cuda.synchronize()
                start = time.time()
                if FLAGS.cuda:
                    target = target.cuda(async=True)
                input_var = torch.autograd.Variable(input)
                target_var = torch.autograd.Variable(target)  # input_var = self.input
                # target_var = self.target
                if local_step == 0:
                    log_fn('Done warm up')
                    log_fn('Step\tImg/sec\tloss')
                    assert len(step_train_times) == self.num_warmup_batches
                    step_train_times = []  # reset to ignore warm up batches
                self.optimizer.zero_grad()
                if self.model_name == 'inception_v3':
                    out, aux = self.model(input_var)
                    out += 0.4 * aux
                else:
                    out = self.model(input_var)
                loss = self.loss(out, target_var)
                loss.backward()
                self.optimizer.step()
                # loss.backward()
                # self.optimizer.step()
                if self.cuda:
                    torch.cuda.synchronize()
                train_time = time.time() - start
                step_train_times.append(train_time)
                if local_step >= 0 and (local_step == 0 or (local_step + 1) % FLAGS.display_every == 0):
                    log_fn('%i\t%s\t%.3f' % (
                        local_step + 1, get_perf_timing_str(self.batch_size, step_train_times), loss.data[0]))
                local_step += 1
        images_per_sec = (local_step - 1.) / np.array(step_train_times)[1:].sum() * self.batch_size
        log_fn('-' * 64)
        log_fn('total images/sec: %.2f' % images_per_sec)
        log_fn('-' * 64)
        store_benchmarks({'total_images_per_sec': images_per_sec})

    def print_info(self):
        """Print basic information."""
        log_fn('Model:       %s' % self.model_name)
        # log_fn('Mode:        %s' % get_mode_from_flags())
        log_fn('Batch size:  %s global' % self.batch_size)
        log_fn('             %s per device' % (self.batch_size / len(self.raw_devices)))
        log_fn('Devices:     %s' % self.raw_devices)
        # log_fn('Optimizer:   %s' % FLAGS.optimizer)
        log_fn('==========')


def get_perf_timing_str(batch_size, step_train_times, scale=1):
    times = np.array(step_train_times)
    speeds = batch_size / times
    speed_mean = scale * batch_size / np.mean(times)
    if scale == 1:
        speed_uncertainty = np.std(speeds) / np.sqrt(float(len(speeds)))
        speed_madstd = 1.4826 * np.median(np.abs(speeds - np.median(speeds)))
        speed_jitter = speed_madstd
        return 'images/sec: %.1f +/- %.1f (jitter = %.1f)' % (
            speed_mean, speed_uncertainty, speed_jitter)
    else:
        return 'images/sec: %.1f' % speed_mean


# cudnn.benchmark = True
def run_benchmarks():
    bench = BenchmarkCNN()
    bench.run()


def manual_seed():
    torch.manual_seed(1234)
    if FLAGS.cuda:
        torch.cuda.manual_seed(1234)


if __name__ == '__main__':
    run_benchmarks()
